@extends('frontend.layouts.blog')

@section('content')
<!-- Banner -->
<div class="youplay-banner banner-top youplay-banner-parallax small">
  <div class="image" style="background-image: url('img/frontend/logo-background.jpg')">
  </div>

  <div class="youplay-user-navigation">
    <div class="container">
      <ul>
        <li><a href="user-activity.html">Activity</a>
        </li>
        <li><a href="user-profile.html">Profile</a>
        </li>
        <li><a href="user-messages.html">Messages <span class="badge">6</span></a>
        </li>
        <li class="active"><a href="user-settings.html">Settings</a>
        </li>
      </ul>
    </div>
  </div>

  <div class="info">
    <div>
      <div class="container youplay-user">
        <a href="images/avatars/{{ $user->avatar }}" class="angled-img image-popup">
          <div class="img">
            <img src="images/avatars/{{ $user->avatar }}">
          </div>
          <i class="fa fa-search-plus icon"></i>
        </a>
            <!--
          -->
          <div class="user-data">
            <h2>{{ $user->name }}</h2>
            <div class="location"><i class="fa fa-map-marker"></i> Los Angeles</div>
            <div class="activity">
              <div>
                <div class="num">69</div>
                <div class="title">Posts</div>
              </div>
              <div>
                <div class="num">12</div>
                <div class="title">Games</div>
              </div>
              <div>
                <div class="num">689</div>
                <div class="title">Followers</div>
              </div>
            </div>
          </div>
        </div>

        <div class="container mt-20">
          <a href="#!" class="btn btn-sm btn-default ml-0">Add Friend</a>
          <a href="#!" class="btn btn-sm btn-default">Private Message</a>
        </div>
      </div>
    </div>
  </div>
  <!-- /Banner -->
  <div class="container youplay-content">


    <div class="row">

      <div class="col-md-9">

        @include('includes.partials.messages')

        <h3 class="mt-0 mb-20">{{ $user->name }}</h3>

        <ul class="pagination pagination-sm mt-0">
          <li class="active">
            <a href="#">General</a>
          </li>
          <li>
            <a href="#">Security</a>
          </li>
          <li>
            <a href="#">Privacy</a>
          </li>
          <li>
            <a href="#">Notifications</a>
          </li>
        </ul>

        {{ Form::model($user, ['route' => 'frontend.user.profile.update', 'class' => 'form-horizontal', 'method' => 'POST']) }}

        <h3>Change Name:</h3>

        <div class="form-group">
          {{ Form::label('name', trans('validation.attributes.frontend.name'), ['class' => 'control-label col-sm-2']) }}
          <div class="col-sm-10">
            <div class="youplay-input">
              {{ Form::input('text', 'name', null, ['placeholder' => trans('validation.attributes.frontend.name')]) }}
            </div>
          </div>
        </div>

        <h3>Change Email:</h3>

        <div class="form-group">
          {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'control-label col-sm-2']) }}
          <div class="col-sm-10"><div class="clearfix"></div>
          <div class="youplay-input">
            {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
          </div>
        </div>
      </div>
      {{ Form::close() }}

      <form action="{{ route('frontend.user.profile.avatar') }}" method="post" enctype="multipart/form-data">

        <h3>Change Avatar:</h3>
        <div class="youplay-input">
        <input type="file" name="avatar">
        </div>

        <button type="submit" class="btn">Update avatar</button>
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <input type="hidden" name="user_id" value="{{ $user->id }}">
      </form>

      {{ Form::open(['route' => ['auth.password.change'], 'class' => 'form-horizontal']) }}
      <h3>Change Password:</h3>
      <div class="form-horizontal mt-30 mb-40">

        <div class="form-group">
          <label class="control-label col-sm-2" for="cur_password">Current Password:</label>
          <div class="col-sm-10">
            <div class="youplay-input">
              {{ Form::input('password', 'old_password', null, ['placeholder' => trans('validation.attributes.frontend.old_password')]) }}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="new_password">New Password:</label>
          <div class="col-sm-10">
            <div class="youplay-input">
              {{ Form::input('password', 'password', null, ['placeholder' => trans('validation.attributes.frontend.new_password')]) }}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="new_password">Confirm New Password:</label>
          <div class="col-sm-10">
            <div class="youplay-input">
              {{ Form::input('password', 'password_confirmation', null, ['placeholder' => trans('validation.attributes.frontend.new_password_confirmation')]) }}
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Change Password</button>
          </div>
        </div>
      </div>
      {{ Form::close() }}

      <h3>Regional Settings:</h3>
      <div class="form-horizontal mt-30 mb-40">
        <div class="form-group">
          <label class="control-label col-sm-2">Language:</label>
          <div class="col-sm-10">
            <div class="youplay-select">
              <select>
                <option>Azərbaycan dili</option>
                <option>Bahasa Indonesia</option>
                <option>Bosanski</option>
                <option>Dansk</option>
                <option>Deutsch</option>
                <option>Eesti</option>
                <option selected>English</option>
                <option>Español</option>
                <option>Esperanto</option>
                <option>Français</option>
                <option>Hrvatski</option>
                <option>Italiano</option>
                <option>Kiswahili</option>
                <option>Latviešu</option>
                <option>Lietuvių</option>
                <option>Magyar</option>
                <option>Moldovenească</option>
                <option>Nederlands</option>
                <option>Norsk</option>
                <option>O‘zbek</option>
                <option>Polski</option>
                <option>Português</option>
                <option>Română</option>
                <option>Shqip</option>
                <option>Slovenščina</option>
                <option>Suomi</option>
                <option>Svenska</option>
                <option>Tagalog</option>
                <option>Tiếng Việt</option>
                <option>Türkmen</option>
                <option>Türkçe</option>
                <option>Čeština</option>
                <option>Ελληνικά</option>
                <option>Адыгэбзэ</option>
                <option>Беларуская</option>
                <option>Български</option>
                <option>ГIалгIай мотт</option>
                <option>Ирон</option>
                <option>Монгол</option>
                <option>Русский</option>
                <option>Српски</option>
                <option>Українська</option>
                <option>ภาษาไทย</option>
                <option>日本語</option>
                <option>臺灣話</option>
                <option>한국어</option>
                <option>پنجابی</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Save Regional Settings</button>
          </div>
        </div>
      </div>


</div><!-- row -->
</div>
</div>
@endsection