<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $guarded = [];

    public function author()
       {
           return $this->belongsTo('App\Models\Access\User\User', 'from_user');
       }   

    public function blog()
    {
        return $this->belongsTo('App\Models\Blog\Blog', 'on_blog');
    }
}
