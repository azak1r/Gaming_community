<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $guarded = [];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Blog\BlogCategory', 'blogs_categories');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Blog\Comments', 'on_blog');
    }

    public function author()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'author_id');
    }
}
