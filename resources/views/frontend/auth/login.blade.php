@extends('frontend.layouts.auth')

@section('content')

<div class="image" style="background-image: url('img/frontend/maxresdefault.jpg')"></div>

<div class="info">
    <div>
      <div class="container align-center">
      @include('includes.partials.messages')
        <div class="youplay-form">
          <h1>{{ trans('labels.frontend.auth.login_box_title') }}</h1>

          <div class="btn-group social-list dib">
            <a class="btn btn-default" title="Share on Facebook" href="#!"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-default" href="#!" title="Share on Twitter"><i class="fa fa-twitter"></i></a>
            <a class="btn btn-default" href="#!" title="Share on Google Plus"><i class="fa fa-google-plus"></i></a>
        </div>

        {{ Form::open(['route' => 'auth.login', 'class' => 'form-horizontal']) }}

        <div class="youplay-input">
            {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
        </div><!--form-group-->

        <div class="youplay-input">
            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
        </div><!--form-group-->

        @if (isset($captcha) && $captcha)
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::captcha() !!}
                {{ Form::hidden('captcha_status', 'true') }}
            </div><!--col-md-6-->
        </div><!--form-group-->
        @endif

        <div class="checkbox">
            <label>
                {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
            </label>
        </div>
        <button class="btn btn-default db">Login</button>

        {{ link_to('password/reset', trans('labels.frontend.passwords.forgot_password')) }}

        {{ Form::close() }}

        <div class="row text-center">
            {!! $socialite_links !!}
        </div>
    </div><!-- panel body -->

</div><!-- panel -->

</div><!-- col-md-8 -->

</div><!-- row -->

@endsection

@section('after-scripts-end')
@if (isset($captcha) && $captcha)
{!! Captcha::script() !!}
@endif
@stop