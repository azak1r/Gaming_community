@extends('frontend.layouts.master')

@section('title')
Home - {{app_name()}}
@endsection
@section('banner')

<!-- Banner -->
<div class="youplay-banner banner-top youplay-banner-parallax">
  <div class="image" style="background-image: url('img/frontend/logo-background.jpg')">
  </div>

  <div class="info">
    <div>
      <div class="container">
        <h2>Welcome<br> to JP IT Gaming</h2>
        <em>"Take a look around and let me know what you think."</em>
      </div>
    </div>
  </div>
</div>
<!-- /Banner -->

@endsection

@section('content')

<div class="col-md-9">
  <div class="news-one">
  @if(!$blogs->count())
  No blog posts here yet. Get authorized for a author account and start creating some.
  @else
    <div class="row vertical-gutter">
      @foreach ($blogs as $post)
      
      <div class="col-md-4">
        <a href="blog-post-1.html" class="angled-img">
          <div class="img">
            <img src="{{$post->thumbnail}}" alt="">
          </div>
        </a>
      </div>
      <div class="col-md-8">
        <div class="clearfix">
          <h3 class="h2 pull-left m-0"><a href="{{ route('blog.single', ['slug' => $post->slug]) }}">{{ $post->title }}</a></h3>
          <span class="date pull-right"><i class="fa fa-user"></i>{{ $post->author->name }} | <i class="fa fa-calendar"></i>{{ $post->created_at }}</span>
        </div>
        <div class="tags">
          <i class="fa fa-tags"></i>  <a href="#">{{ $post->category }}</a>
        </div>
        <div class="description">
          <p>
            {!! $post->body !!}
          </p>
        </div>
        <a href="{{ route('blog.single', ['slug' => $post->slug]) }}" class="btn read-more pull-left">Read More</a>
      </div>
      <div class="clearfix"></div>
      
      @endforeach
    </div>
    @endif
  </div>

  <!-- Pagination -->

  @if($blogs->lastpage() > 1)
  <ul class="pagination">
    @if($blogs->currentPage() !== 1)
    <li><a href="{{ $blogs->previousPageUrl() }}"><i class="fa fa-caret-left"></i></a></li>
      @endif
      @if($blogs->currentPage() !== $blogs->lastPage())
      <li><a href="{{ $blogs->nextPageUrl() }}"><i class="fa fa-caret-right"></i></a></li>
        @endif
      </ul>
      @endif

    </div>

    <!-- Right Side -->
    <div class="col-md-3">

      <!-- Side Search -->
      <div class="side-block">
        <p>Search by News:</p>
        <form action="search.html">
          <div class="youplay-input">
            <input type="text" name="search" placeholder="enter search term">
          </div>
        </form>
      </div>
      <!-- /Side Search -->

    </div>
    <!-- /Right Side -->

  </div>

  @endsection