@extends('backend.layouts.master')

@section('page-header')
<h1>
    {{ app_name() }}
    <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') }} {{ access()->user()->name }}!</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        {!! getLanguageBlock('backend.lang.welcome') !!}
    </div><!-- /.box-body -->
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        {!! history()->render() !!}
    </div><!-- /.box-body -->
</div><!--box box-success-->

<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Blog Updates </h3>&nbsp;
        <div class="btn-group" role="group">
            <a href="{{ route('admin.blog.create_post') }}" class="btn btn-info">New Blog Post</a>
        </div>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <ul>
            @if(count($blogs) == 0)
            <li>No new Blogs</li>
            @else
            @foreach ($blogs as $post)
            <article>
                <div class="post-info">
                    <h3>{{ $post->title }}</h3>  
                    <span class="info">Author: {{ $post->author->name }} | Created at: {{ $post->created_at }}</span>             
                    <nav>
                        <a href="{{ route('admin.blog.post', ['slugh' => $post->slug, 'end' => 'backend']) }}">View</a></li>
                        <a href="{{ route('admin.blog.post.edit', ['post_id' =>$post->id]) }}">Edit</a>
                        <a href=""><span style="color:red">Delete</span></a>
                    </nav>           
                </div>
            </article>
            @endforeach
            @endif
        </ul>
    </div>        
</div>

@endsection