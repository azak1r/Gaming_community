<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Blog\Blog;
use App\Models\Blog\Category;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->take(3)->get();
        return view('backend.dashboard', compact('blogs'));
    }
}