<?php

Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
Route::get('/blog/posts/create', ['uses' => 'BlogController@getCreateBlog', 'as' => 'admin.blog.create_post']);
Route::post('/blog/post/create', ['uses' => 'BlogController@postCreateBlog', 'as' => 'admin.blog.post.create']);
Route::get('/blogs/posts', ['uses' => 'BlogController@getBlogIndex', 'as' => 'admin.blog.index']);
Route::get('blog/post{slug}&{end}', ['uses' => 'BlogController@getSingleBlog', 'as' => 'admin.blog.post']);
Route::get('blog/{slug}/edit', ['uses' => 'BlogController@getUpdateBlog', 'as' => 'admin.blog.post.edit']);
Route::post('/blog/post/update', ['uses' => 'BlogController@postUpdateBlog', 'as' => 'admin.blog.post.update']);