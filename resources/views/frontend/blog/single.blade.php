@extends('frontend.layouts.blog')

@section('title')
{{ $blog->title }} - {{app_name()}}
@endsection
@section('content')

<!-- Banner -->
<div class="youplay-banner banner-top youplay-banner-parallax xsmall">
  <div class="image" style="background-image: url('{{$blog->thumbnail}}')">
  </div>

  <div class="info">
    <div>
      <div class="container">
        <h1>{{$blog->title}}</h1>
      </div>
    </div>
  </div>
</div>
<!-- /Banner -->

<div class="container youplay-news youplay-post">

  <div class="container youplay-news youplay-post">
    <article class="news-one">
      <div class="description">
        <p>
          {!!$blog->body!!}
        </p>
      </div>

      <div class="tags">
        <i class="fa fa-tags"></i>  tags
      </div>

      <div class="meta">
        <div class="item">
          <i class="fa fa-user meta-icon"></i>
          Author: <a href="#!">{{$blog->author->name}}</a>
        </div>
        <div class="item">
          <i class="fa fa-calendar meta-icon"></i>
          Published: <a href="#!">{{$blog->created_at}}</a>
        </div>
        <div class="item">
          <i class="fa fa-bookmark meta-icon"></i>
          Category: <a href="#!">{{$blog->category}}</a>
        </div>
      </div>

      <div class="btn-group social-list social-likes" data-counters="no">
        <span class="btn btn-default facebook" title="Share link on Facebook"></span>
        <span class="btn btn-default twitter" title="Share link on Twitter"></span>
        <span class="btn btn-default plusone" title="Share link on Google+"></span>
      </div>
    </article>
    <div class="comments-block">
    <h2>Comments <small>({{ $comments->count() }})</small></h2>
      
      <!-- Comment Form -->
      @if(Auth::check())
      <h2>Leave a reply?</h2>
      <form method="POST" action="{{route('blog.comment')}}" class="comment-form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="on_blog" value="{{ $blog->id }}">
        <input type="hidden" name="slug" value="{{ $blog->slug }}">
        <div class="comment-cont clearfix">
          <div class="youplay-textarea">
            <textarea required="required" placeholder="Enter comment here" name = "body" class="form-control"></textarea>
          </div>
          <button class="btn btn-default pull-right" type="submit" name='post_comment' value = "Post">Submit</button>
        </div>
      </form>
      @else
      <h2>Want to leave a reply?</h2>
      <a href="{{ route('auth.register') }}" class="btn btn-info">Register </a> or <a href="{{ route('auth.login') }}" class="btn btn-info">Login</a>
      @endif
      <!-- /Comment Form -->
    
    <!-- /Post Comments -->
    <ul class="comments-list">
      @foreach($comments as $comment)
      <li>
        <article>
          <div class="comment-avatar pull-left">
            <img src="/images/avatars/{{$comment->author->avatar}}" alt="">
          </div>
          <div class="comment-cont clearfix">
            <a class="comment-author h4" href="#!">{{ $comment->author->name }}</a>
            <div class="date">
              <i class="fa fa-calendar"></i> {{ $comment->created_at->diffForHumans() }}
            </div>
            <div class="comment-text">
              {{ $comment->body }}
            </div>
          </div>
        </article>
      </li>
      @endforeach

    </ul>
  </div>

</div>

@endsection