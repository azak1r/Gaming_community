<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="_token" content="{{ csrf_token() }}" />
  <title>@yield('title')</title>
  
  <meta name="description" content="Gaming Community Web Platform designed by JP IT">
  <meta name="author" content="Joery Pigmans">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Icon -->
  <link rel="icon" type="image/png" href="{{ asset('img/icon.png') }}">
  
  <!-- Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
  
  {{ Html::style('css/fonts/font-awesome/font-awesome.min.css') }}
  {{ Html::style('css/bootstrap.min.css') }}
  {{ Html::style(elixir('css/frontend.css')) }}

  <!--[if lt IE 9]>
      <script src="../assets/plugins/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
    </head>
    <body>
      @include('frontend.includes.nav')

      <section class="content-wrap full youplay-login">
       

       
        <div class="youplay-banner banner-top">
        
          @yield('content')
        </div>
        
      </section>
      <!-- /Main Content -->
      <!-- Scripts -->
      {!! Html::script('js/vendor/jquery/jquery.min.js') !!}
      {!! Html::script('js/vendor/bootstrap/bootstrap.min.js') !!}
      {!! Html::script(elixir('js/frontend.js')) !!}

      <!-- Initialize youplay effects -->
      <script>
        if(typeof youplay !== 'undefined') {
          youplay.init({
            parallax:         true,
            navbarSmall:      false,
            fadeBetweenPages: true,
            smoothscroll: true,
          });
        }
      </script>

    </body>
    </html>