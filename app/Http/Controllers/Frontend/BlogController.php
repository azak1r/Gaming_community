<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog\Blog;

/**
 * Class FrontendBlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function getBlogIndex() 
    {
        $blogs = Blog::where('active',1)->orderBy('created_at', 'desc')->paginate(5);
        return view('frontend.blog.index', compact('blogs'));
    }

    public function getSingleBlog($slug, $end = 'frontend')
    {
        $blog = Blog::where('slug', $slug)->first();
        if(!$blog) {
            return redirect()->route('blog.index')->with(['flash_warning' => 'Blog post not found!']);
        }
        $comments = $blog->comments;

        return view($end . '.blog.single', ['blog' => $blog],  compact('comments'));
    }

    public function comment()
    {
        return $this->hasMany('App\Models\Blog\Comment');
    }

}