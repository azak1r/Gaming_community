@extends('frontend.layouts.auth')

@section('content')

<div class="image" style="background-image: url('img/frontend/register.png')"></div>

<div class="info">
    <div>
      <div class="container align-center">
      @include('includes.partials.messages')
        <div class="youplay-form">
          <h1>{{ trans('labels.frontend.auth.register_box_title') }}</h1>

          <div class="btn-group social-list dib">
            <a class="btn btn-default" title="Share on Facebook" href="#!"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-default" href="#!" title="Share on Twitter"><i class="fa fa-twitter"></i></a>
            <a class="btn btn-default" href="#!" title="Share on Google Plus"><i class="fa fa-google-plus"></i></a>
        </div>

        {{ Form::open(['route' => 'auth.register', 'class' => 'form-horizontal']) }}

        <div class="youplay-input">
            {{ Form::input('name', 'name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.name')]) }}
        </div>

        <div class="youplay-input">
            {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
        </div>

        <div class="youplay-input">
            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
        </div>

        <div class="youplay-input">
            {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
        </div>

        @if (config('access.captcha.registration'))
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::captcha() !!}
                {{ Form::hidden('captcha_status', 'true') }}
            </div><!--col-md-6-->
        </div><!--form-group-->
        @endif

        <button class="btn btn-default db">{{ Form::submit(trans('labels.frontend.auth.register_button'), ['class' => 'btn btn-primary']) }}</button>

        {{ Form::close() }}

    </div><!-- panel body -->

</div><!-- panel -->

</div><!-- col-md-8 -->

</div><!-- row -->
@endsection

@section('after-scripts-end')
@if (config('access.captcha.registration'))
{!! Captcha::script() !!}
@endif
@stop